# Operating-Systems-Silberschatz
Operating Systems are an essential part of any computer system. Similarly a course on operating System is an essential part of any computer-science education. This field is undergoing rapid change as computers are now prevalent in virtually every application, from games for children through the most sophisticated planning tools for government and multinational firms


Table Of Contents:

1. Overview
2. Process Management
3. Process Co-ordination
4. Memory Management
5. Storage Management
6. Protection and Security
7. Distributed Systems
8. Special Purpose Systems
9. Case Studies

